Anywhere
===

启动运行HTTP Server，并将当前目录设置为静态文件根目录。

## [Rust](rust/README.md)

## [Java](java/README.md)

## [Go](go/README.md)

## [Scala](sava/README.md)

## [JavaScript](javascript/README.md) 

## [Clojure](clojure/README.md)

## [Erlang](erlang/README.md)
