## 安装[Golang](http://golang.org)

## 修改

+ 升级到`go 1.14`版本，使用`go mod`管理程序

## 运行

+ `go run`

## 发布

```shell
go build -o bin/anywhere
bin/anywhere -h
```
