## 安装[JDK 17](http://www.oracle.com)

## [Gradle](https://gradle.org)

```shell
gradle -q fatJar -x test
```

## 启动运行

+ 复制`Gradle:build/libs`或者`SBT:target\scala-2.11`目录下`mod-anywhere-assembly-x.y.z.jar`文件到根目录，执行命令：`java -jar mod-anywhere-assembly-x.y.z.jar -h 127.0.0.1 -p 80 -c`
+ 访问:`http://127.0.0.1/`
+ 参数：主机名：`-h` `--host` 默认为`localhost`、服务端口：`-p` `--port`默认为`8080`、缓存：`-c` `--cache`默认为不缓存。


## 修改记录

+ 修改为使用 `Java 17`;

+ 修改为使用 `Java 11` 中依然保留的 `com.sun.net.httpserver.HttpServer` 构建；
