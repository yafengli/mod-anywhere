package io.anywhere;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command
public class CliOptions {
  @Option(names = { "-h", "--help" }, usageHelp = true, description = " help message")
  public boolean help = false;
  @Option(names = { "-s", "--server" }, description = " server address")
  public String server = "127.0.0.1";
  @Option(names = { "-p", "--port" }, description = " server port")
  public int port = 80;
  @Option(names = { "-d", "--dir" }, description = "static file directory")
  public String dir = System.getProperty("user.dir");
}
