# Rust 

## 构建

```shell

cargo build --release
```

## 运行

- `Windows` 可执行文件 `target\release\mod-anywhere.exe`。
- `Linux` 可执行文件 `target\release\mod-anywhere`。

```shell
{可执行文件} -d /tmp/static -a 127.0.0.1:3000
```
