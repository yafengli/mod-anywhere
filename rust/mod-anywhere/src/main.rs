use clap::Parser;

mod d_axum;
mod d_tcp;

#[derive(Parser)]
#[command(author,version = None, about,long_about=None)]
struct Args {
    /// 静态文件目录
    #[arg(short, long, default_value_t=String::from("/tmp/static"))]
    dir: String,

    /// 运行服务地址
    #[arg(short, long, default_value_t=String::from("127.0.0.1:3000"))]
    addr: String,
}

#[tokio::main]
async fn main() {
    let arg = Args::parse();
    println!("Serve:[{}] http://{}/index.html", arg.dir, arg.addr);

    //d_tcp::demo_http(arg.dir.as_str(), arg.addr.as_str());
    d_axum::static_file_serve(arg.dir.as_str(), arg.addr.as_str()).await;
}
