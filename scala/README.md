## 安装[JDK](http://www.oracle.com)

## [SBT](http://www.scala-sbt.org)

```shell
sbt 
$ assembly
```

## 启动运行

+ 执行命令：`java -jar target\scala-2.13\mod-anywhere-assembly-x.y.z.jar`
+ 参数：主机名：`-h` `--host` 默认为`127.0.0.1`、服务端口：`-p` `--port`默认为`80`、缓存：`-c` `--cache`默认为不缓存。

## 修改记录

+ 修改运行环境 `JDK 17`，修改构建工具 `SBT 1.9.2`
