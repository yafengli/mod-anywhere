import Build._

name := """mod-anywhere"""

organization := """io.anywhere"""

version := $("prod")

scalaVersion := $("scala")

mainClass in assembly := Some("io.anywhere.Boot")

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "io.netty.versions.properties") => MergeStrategy.discard
  case x => MergeStrategy.defaultMergeStrategy(x)
}

libraryDependencies ++= Seq(
  "com.beust" % "jcommander" % $("jcommander"),
  "com.typesafe.akka" %% "akka-http" % $("akka_http"),  
  "com.typesafe.akka" %% "akka-stream" % $("akka"),
  "org.scalatest" %% "scalatest" % $("scalatest") % "test",
  "junit" % "junit" % $("junit") % "test"
)
