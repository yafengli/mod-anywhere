import java.io.File
import sbt._
import scala.collection.mutable

object Build {
  val reg = "(.+)=(.+)".r 
  val $ = scala.io.Source.fromFile(new File("gradle.properties")).getLines()
    .filter(reg.findFirstMatchIn(_).isDefined).map(reg.findFirstMatchIn(_).get).map(m => (m.group(1) -> m.group(2))).toMap
}
