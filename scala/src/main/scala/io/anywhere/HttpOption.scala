package io.anywhere

import com.beust.jcommander.Parameter

class HttpOption {
  @Parameter(names = Array("-h", "--host"), description = "主机名")
  var host: String = "127.0.0.1"
  @Parameter(names = Array("-p", "--port"), description = "服务端口")
  var port: Int = 8080
  @Parameter(names = Array("-c", "--cache"), description = "缓存")
  var cache: Boolean = false
}
